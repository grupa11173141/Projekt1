import sqlite3 as sql

def PołKur(adres):
    #Tworzy połącznie z wybranym plikiem bazy danych i kursor.
    global Połączenie
    Połączenie = sql.connect(adres)
    global Kursor
    Kursor = Połączenie.cursor()

def WyświetlWszystko():
    #Wświetla wszystkie wpisy w bazie danych.
    Kursor.execute("SELECT * FROM Lista;")
    zbiór=Kursor.fetchall()
    for wiersz in zbiór:
        print(wiersz)

def NowyKontakt():
    #Dodaje nowy kontakt do listy.
    print("Podaj dane. (\"-\" żeby pominąć, \"x\" żeby przerwać)")
    Imi=input("Imię?\n")
    if Imi=="x":
        return
    Nazw=input("Nazwisko?\n")
    if Nazw=="x":
        return
    Tel=(input("Nr telefonu?\n"))
    if Tel=="x":
        return
    elif Tel=="-":
        Tel=None
    else:
        Tel=int(Tel)
    Ema=input("Adres e-mail?\n")
    if Ema=="x":
        return

    dane = [Imi, Nazw, Tel, Ema]

    for i in range(4):
        if dane[i]=="-":
            dane[i]=None


    Kursor.execute("INSERT INTO Lista (Imie, Nazwisko,NrTel, Email) VALUES(?,?,?,?)", dane)
    Połączenie.commit()

def UsuńKontakt():
    #Usuwa wskazany kontakt.
    print("Który kontakt usunąć? (\"x\" aby przerwać)")
    usu=input()
    if usu=="x":
        return
    else:
        Kursor.execute("DELETE FROM Lista WHERE NrOsoby=?", usu)

def ZmieńKontakt():
    #Zmienia wartość wybranego pola.
    Numer=input("Który kontakt? (\"-\" aby usunąć, \"x\" aby przerwać)\n")
    if Numer=="x":
        return
    Pole=input("Które pole?\nImię - 1, Nazwisko - 2, Nr Telefonu - 3, E-mail - 4\n")
    if Pole=="x":
        return
    Nowe=input("Podaj nową wartość:\n")
    if Nowe=="x":
        return
    elif Nowe=="-":
        Nowe=None

    dane=[Nowe, Numer]
    if Pole=="1":
        Kursor.execute("UPDATE Lista SET Imie=? WHERE NrOsoby=?", dane)
    elif Pole=="2":
        Kursor.execute("UPDATE Lista SET Nazwisko=? WHERE NrOsoby=?", dane)
    elif Pole=="3":
        Kursor.execute("UPDATE Lista SET NrTel=? WHERE NrOsoby=?", dane)
    else:
        Kursor.execute("UPDATE Lista SET Email=? WHERE NrOsoby=?", dane)
    Połączenie.commit()

PlikBazy="baza.db"

PołKur(PlikBazy)
WyświetlWszystko()

#Menu

Wybór=0
while Wybór!=9:
    print("Wyświetl wszystko - 1")
    print("Nowy kontakt - 2")
    print("Usuń kontakt - 3")
    print("Zmień kontakt - 4")
    print("Zakończ - 9")
    Wybór=int(input())
    if Wybór==1:
        WyświetlWszystko()
    elif Wybór==2:
        NowyKontakt()
    elif Wybór==3:
        UsuńKontakt()
    elif Wybór==4:
        ZmieńKontakt()
    else:
        pass


#Zamknięcie połączenia.
Kursor.close()
Połączenie.close()